<%-- 
    Document   : consultaVehiculo
    Created on : 6/10/2021, 6:21:38 p. m.
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Easy Parking - Consulta de vehiculos</title>
  </head>
  <body>

    <div class="container-fluid px-2 py-2">
        <div class="container round-border solid-border gray-border text-center">
            <h1 style="font-size: 50px; margin: 20px;">BSH-039</h1>            
            <hr>
            <img src="https://cdn-icons-png.flaticon.com/512/3089/3089803.png" alt="" style="width: 200px;">
            <hr>
            <div class="row">
                <h3 class="col-sm-6 col-12 no-margin">Fecha de ingreso:</h3>
                <p class="col-sm-6 col-12 no-margin">14/09/2021</p>
            </div>        
            <hr>
            <div class="row">
                <h3 class="col-sm-6 col-12 no-margin">Hora de ingreso:</h3>
                <p class="col-sm-6 col-12 no-margin">17:34</p>
            </div>   
            <hr>
            <div class="container-fluid">
                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-clock center" viewBox="0 0 16 16">
                    <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                </svg>
                <h2>2:34:20</h2>
            </div>
            <hr>
            <h1 style="font-size: 50px; margin: 20px;">5,200$</h1>  
        </div>        
        <div class="container text-center mt-2 p-0">
            <a href="index.jsp"><button class="btn btn-warning col-12">Atras</button></a>
        </div>
        
    </div>    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    -->
  </body>
</html>
