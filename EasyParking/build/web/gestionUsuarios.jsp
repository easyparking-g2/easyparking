<%-- 
    Document   : gestionUsuarios
    Created on : 6/10/2021, 6:21:50 p. m.
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import ="ModeloDAO.*" %>
<%@ page import ="Modelo.usuario" %>
<%@ page import ="java.util.List" %>
<%@ page import ="java.util.Iterator" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Easy Parking - Gestion de usuarios</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-2">
        <div class="container-fluid">
            <a class="navbar-brand" style="font-family: 'Righteous', cursive;" href="#"><span class="yellow-text">E</span>asy <span class="yellow-text">P</span>arking</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#">Parqueadero</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Registros</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Usuarios</a>
                    </li>   
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Cerrar sesion</a>
                    </li>            
                </ul>
            </div>
        </div>    
    </nav>

    <div class="container-fluid px-2">
        <div class="container-xxl round-border solid-border gray-border mb-2">
            <h2 class="my-4">Agregar Usuario</h2>
            <form class="row g-1 my-1" action="Controlador">
                <div class="container col-md-4">
                    <input type="text" placeholder="Usuario" class="form-control" name="usuTxt">
                </div>   
                <div class="container col-md-4">
                    <input type="password" placeholder="Contraseña" class="form-control" name="conTxt">
                </div>              
                <select id="select" name="select" class="col-md-4 gray-border round-border">
                    <option value="Cajero">Cajero</option>
                    <option value="Administrador">Administrador</option>                
                </select>
                <input class="btn btn-warning mb-1" type="submit" name="accion" value="agregar">                
            </form>                            
        </div>

        <div class="container-xxl round-border solid-border gray-border">
            <h2 class="my-4">Usuarios</h2>
            
            <div class="container-fluid round-border solid-border gray-border mb-3 py-3">     
                <%
                    usuarioDAO dao = new usuarioDAO();
                    List<usuario> list = dao.listar();
                    Iterator<usuario> iter = list.iterator();
                    usuario usu = null;
                    while(iter.hasNext())
                    {
                        usu = iter.next();                                            
                %>
                <form class="container-fluid" action="Controlador">
                    <div class="row">
                        <input type="text" class="form-control" name="usuId" value="<%= usu.getId() %>" readonly hidden>
                        <p class="col-sm-4 my-auto"><%= usu.getNombre()%></p>
                        <div class="d-flex container col-sm-8 justify-content-end">
                            <button class="btn btn-warning mx-1" style="width: 50px; height: 50px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                                    <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                                  </svg>
                            </button>                            
                            <button type="submit" name="accion" value="eliminar" class="btn btn-danger" style="width: 50px; height: 50px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-x" viewBox="0 0 16 16">
                                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                    <path fill-rule="evenodd" d="M12.146 5.146a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
                                  </svg>
                            </button>                            
                        </div>                        
                    </div>                    
                </form>
                <hr>
                <%}%>
            </div>          
                        
        </div>
    </div>    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    </body>
</html>
