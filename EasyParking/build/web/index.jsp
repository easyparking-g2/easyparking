<%-- 
    Document   : index
    Created on : 6/10/2021, 6:15:59 p. m.
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
        
        <title>Easy Parking - Login</title>
    </head>
    <body>
        <h1 class="text-center"><span class="yellow-text">E</span>asy <span class="yellow-text">P</span>arking</h1>

    <div class="container-fluid px-2">   
        <div class="container round-border solid-border gray-border text-center">

            <div class="container my-4">
                <p>Consulta el valor de tu tiempo en el parqueadero</p>            
                <input type="text" placeholder="Placa del vehiculo" class="form-control mb-2">            
                <a href="consultaVehiculo.jsp"><button class="btn btn-warning col-12">Consultar</button></a>
            </div>

            <hr>

            <div class="container my-4">
                <p>Ingresa con tu cuenta de funcionario</p>
                <input type="text" placeholder="Usuario" class="form-control mb-2">
                <input type="password" placeholder="Contraseña" class="form-control mb-2">
                <a href="gestionUsuarios.jsp"><button class="btn btn-warning col-12">Ingresar</button></a>
            </div>
        </div>
    </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        
    </body>        
</html>
