/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Interfaces;

import Modelo.usuario;
import java.util.List;

/**
 *
 * @author Oscar
 */
public interface CRUD 
{
    public List listar();
    public boolean agregar(usuario usu);
    public boolean eliminar(int nombre);
}
