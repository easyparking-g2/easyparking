/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Oscar
 */
public class usuario 
{
    int id, tipoUsuario;
    String nombre, contraseña;

    public usuario() {
    }

    public usuario(int id, int tipoUsuario, String nombre, String contraseña) {
        this.id = id;
        this.tipoUsuario = tipoUsuario;
        this.nombre = nombre;
        this.contraseña = contraseña;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombreSinTipo() {
        return nombre;
    }

    public String getNombre() {
        return nombre + " - " + ((tipoUsuario == 0)? "Cajero":"Administrador");
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public String getContraseña() {
        return contraseña;
    }
    
    
}
