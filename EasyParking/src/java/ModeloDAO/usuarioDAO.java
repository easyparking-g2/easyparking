/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ModeloDAO;

import BaseDatos.Conexion;
import Interfaces.CRUD;
import Modelo.usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oscar
 */
public class usuarioDAO implements CRUD
{
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    
    @Override
    public List listar() {
        
        ArrayList<usuario> lista = new ArrayList<>();
        String sql = "select * from usuarios";
        try
        {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next())
            {
                usuario usu = new usuario( rs.getInt("ID"), rs.getInt("TipoUsuario"), rs.getString("Nombre"), rs.getString("Contraseña"));
                lista.add(usu);
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error: " + ex.getMessage());
        }
        
        return lista;
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.        
    }

    @Override
    public boolean agregar(usuario usu) 
    {
        String sql = "insert into usuarios values (default,'"+usu.getNombreSinTipo()+"','"+usu.getContraseña()+"','"+usu.getTipoUsuario()+"')";
        try
        {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            
            return true;
        }
        catch(Exception ex)
        {
            System.err.println("Error: " + ex.getMessage());
            return false;
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(int id) 
    {
        String sql = "delete from usuarios where id = "+id+";";
        try
        {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            
            return true;
        }
        catch(Exception ex)
        {
            throw new UnsupportedOperationException("Error: " + ex.getMessage());
            //System.err.println("Error: " + ex.getMessage());
            //return false;
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
