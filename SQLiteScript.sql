CREATE DATABASE EasyParking;

CREATE TABLE TipoVehiculo
(
ID int primary key auto_increment,
Tipo varchar(50) not null,
Img_url varchar(500) not null
);
/*
CREATE TABLE VehiculosTemp
(
Placa varchar(10) primary key,
Fecha date not null,
Hora time not null,
TipoVehiculo int references TipoVehiculo(ID) not null
)
*/
CREATE TABLE Tarifas
(
ID int primary key auto_increment,
NombreTarifa varchar(50) not null,
TipoVehiculo int not null references TipoVehiculo(ID),
ValorTarifa int not null
);

CREATE TABLE Usuarios
(
ID int primary key auto_increment,
Nombre varchar(100) not null,
Contraseña varchar(20) not null,
TipoUsuario int not null
);

CREATE TABLE RegistroFacturacion
(
ID int primary key auto_increment,
Placa varchar(10) not null,
TipoVehiculo int not null references TipoVehiculo(ID),
FechaIngreso date not null,
HoraIngreso time not null,
FechaSalida date,
HoraSalida time,
ValorTarifa int not null,
IVA int not null,
Total int not null,
RegistradoPor int references Usuarios(ID)
);
/*
DROP TABLE RegistroFacturacion
DROP TABLE VehiculosTemp
*/

insert into usuarios values (default,'Oscar Diaz','Oscar','1');
insert into usuarios values (default,'Jorgue Rodriguez','Jorgue','1');
insert into usuarios values (default,'Santiago Ulloa','Santiago','0');
insert into usuarios values (default,'Diego Tellez','Diego','0');
update usuarios set nombre = 'Oscar Diaz' where id = 1;

select*from usuarios;